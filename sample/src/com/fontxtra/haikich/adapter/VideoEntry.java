package com.fontxtra.haikich.adapter;

public class VideoEntry {

	private String text;
    private String videoId;
    public VideoEntry() {
        this.setText("");
        this.setVideoId("");
      }
    public VideoEntry(String text, String videoId) {
      this.setText(text);
      this.setVideoId(videoId);
    }
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}


}
