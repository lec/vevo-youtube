/*
olo * Copyright 2012 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fontxtra.haikich.activities;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.collections.map.LinkedMap;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.darvds.ribbonmenu.RibbonMenuView;
import com.darvds.ribbonmenu.iRibbonMenuCallback;
import com.fasterxml.jackson.databind.JsonNode;
import com.fontxtra.haikich.adapter.VideoEntry;
import com.fontxtra.haikich.utils.Converter;
import com.fontxtra.haikich.utils.HttpUtil;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnFullscreenListener;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailLoader.ErrorReason;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

/**
 * A sample Activity showing how to manage multiple YouTubeThumbnailViews in an adapter for display
 * in a List. When the list items are clicked, the video is played by using a YouTubePlayerFragment.
 * <p>
 * The demo supports custom fullscreen and transitioning between portrait and landscape without
 * rebuffering.
 */
@TargetApi(13)
public final class VideoListDemoActivity extends Activity implements OnFullscreenListener, iRibbonMenuCallback {

  /** The duration of the animation sliding up the video in portrait. */
  private static final int ANIMATION_DURATION_MILLIS = 300;
  /** The padding between the video list and the video in landscape orientation. */
  private static final int LANDSCAPE_VIDEO_PADDING_DP = 5;
  private RibbonMenuView rbmView;
  /** The request code when calling startActivityForResult to recover from an API service error. */
  private static final int RECOVERY_DIALOG_REQUEST = 1;
  public String HOST_VEVO = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PLWRJVI4Oj4IaYIWIpFlnRJ_v_fIaIl6Ey&key=AIzaSyDkgIJry9vJ1Mhbu-2Zw9VUP57K3lvh4PE";
  private VideoListFragment listFragment;
  private VideoFragment videoFragment;
  public static List<VideoEntry> VIDEO_LIST = new ArrayList<VideoEntry>();
  public static List<VideoEntry> movies = new ArrayList<VideoEntry>();
  public static LayoutInflater inflator;
  private View videoBox;
  private View closeButton;
  private View playNowButton;
  
  private boolean isFullscreen;
  public static int adId = 65832139;
  private ProgressDialog progress;
  @SuppressLint("NewApi")
  private void loadRightMenu(){
	  rbmView = (RibbonMenuView) findViewById(R.id.ribbonMenuView1);
      rbmView.setMenuClickCallback(this);
      rbmView.setMenuItems(R.menu.ribbon_menu);
      getActionBar().setDisplayHomeAsUpEnabled(true);
      getActionBar().setHomeButtonEnabled(true);

  }
  private void showPleaseWait()
	{
		progress = ProgressDialog.show(this, getString(R.string.loadingTilte),
      	    getString(R.string.loadingWelcome), true);
	}
	private void hidePleaseWait()
	{
		if (progress != null)
  	{
  		progress.dismiss();
  		progress = null;
  	}
	}
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.video_list_demo);
    showPleaseWait();
    inflator = (LayoutInflater) this
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View v = inflator.inflate(R.layout.paging, null);
    
    getActionBar().setDisplayShowCustomEnabled(true);
    getActionBar().setCustomView(v);
    ImageButton back = (ImageButton)getActionBar().getCustomView().findViewById(R.id.backButton);
    back.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Log.i("back", listFragment.nextPage);
			if (listFragment.prevPage != ""){
				showPleaseWait();
				String url = getUrl(listFragment.query, listFragment.prevPage, DeveloperKey.DEVELOPER_KEY);
				GetMovies gm = new GetMovies();
				gm.execute(url);
			}
		}
	});
    ImageButton next = (ImageButton)getActionBar().getCustomView().findViewById(R.id.nextButton);
    next.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (listFragment.nextPage != ""){
				Log.i("next", listFragment.nextPage);
				showPleaseWait();
				String url = getUrl(listFragment.query, listFragment.nextPage, DeveloperKey.DEVELOPER_KEY);
				GetMovies gm = new GetMovies();
				gm.execute(url);
			}
		}
	});
    
    listFragment = (VideoListFragment) getFragmentManager().findFragmentById(R.id.list_fragment);
    videoFragment =
        (VideoFragment) getFragmentManager().findFragmentById(R.id.video_fragment_container);
    videoBox = findViewById(R.id.video_box);
    closeButton = findViewById(R.id.close_button);
    playNowButton = findViewById(R.id.playNow_button);

    videoBox.setVisibility(View.INVISIBLE);
    loadAdview();
    layout1();
    loadRightMenu();
    checkYouTubeApi();
    listFragment.query = "hoai+linh";
	String st = VideoListDemoActivity.getUrl(listFragment.query, "", DeveloperKey.DEVELOPER_KEY);
    GetMovies gm = new GetMovies();
    gm.execute(st);
  }
  private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlist);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(adId);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
  private void checkYouTubeApi() {
    YouTubeInitializationResult errorReason =
        YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(this);
    if (errorReason.isUserRecoverableError()) {
      errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
    } else if (errorReason != YouTubeInitializationResult.SUCCESS) {
      String errorMessage =
          String.format(getString(R.string.error_player), errorReason.toString());
      Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == RECOVERY_DIALOG_REQUEST) {
      // Recreate the activity if user performed a recovery action
      recreate();
    }
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);

    layout1();
  }

  @Override
  public void onFullscreen(boolean isFullscreen) {
    this.isFullscreen = isFullscreen;
    Log.v("click", "click fullscreen");
    layout1();
  }
  private static long back_pressed;
  @SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		loadAdview();
		if(isFullscreen){
			isFullscreen = false;
		}
		layout1();
		// If the user is looking at detailed rows, put them back to the other screen instead
		// of leaving the page entirely
		if (back_pressed + 2000 > System.currentTimeMillis())
		{
			super.onBackPressed();
		}
		else
		{
			Toast.makeText(this, "Double click to close", Toast.LENGTH_SHORT).show();
			back_pressed = System.currentTimeMillis();
		}
	}
  /**
   * Sets up the layout programatically for the three different states. Portrait, landscape or
   * fullscreen+landscape. This has to be done programmatically because we handle the orientation
   * changes ourselves in order to get fluent fullscreen transitions, so the xml layout resources
   * do not get reloaded.
   */
  private void layout1() {
    boolean isPortrait =
        getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlist);
//    mBanner.setVisibility(isFullscreen ? View.GONE : View.VISIBLE);
    listFragment.getView().setVisibility(isFullscreen ? View.GONE : View.VISIBLE);
    listFragment.setLabelVisibility(isPortrait);
    closeButton.setVisibility(isPortrait ? View.VISIBLE : View.GONE);
    playNowButton.setVisibility(isPortrait ? View.VISIBLE : View.GONE);
    
    if (isFullscreen) {
      videoBox.setTranslationY(0); // Reset any translation that was applied in portrait.
      mBanner.setVisibility(View.GONE);
      setLayoutSize(videoFragment.getView(), MATCH_PARENT, MATCH_PARENT);
      setLayoutSizeAndGravity(videoBox, MATCH_PARENT, MATCH_PARENT, Gravity.FILL);
    } else if (isPortrait) {
    	mBanner.setVisibility(View.VISIBLE);
      setLayoutSize(listFragment.getView(), MATCH_PARENT, MATCH_PARENT);
      setLayoutSize(videoFragment.getView(), MATCH_PARENT, WRAP_CONTENT);
      setLayoutSizeAndGravity(videoBox, MATCH_PARENT, WRAP_CONTENT, Gravity.BOTTOM);
    } else {
      videoBox.setTranslationY(0); // Reset any translation that was applied in portrait.
      mBanner.setVisibility(View.VISIBLE);
      int screenWidth = dpToPx(getResources().getConfiguration().screenWidthDp);
      setLayoutSize(listFragment.getView(), screenWidth / 4, MATCH_PARENT);
      int videoWidth = screenWidth - screenWidth / 4 - dpToPx(LANDSCAPE_VIDEO_PADDING_DP);
      setLayoutSize(videoFragment.getView(), videoWidth, WRAP_CONTENT);
      setLayoutSizeAndGravity(videoBox, videoWidth, WRAP_CONTENT,
          Gravity.RIGHT | Gravity.CENTER_VERTICAL);
    }
  }
  @Override
	public boolean onOptionsItemSelected(MenuItem item) {
	  switch (item.getItemId()) {
	  case android.R.id.home:
		rbmView.toggleMenu();
		return true;
	  }
	  return super.onOptionsItemSelected(item);
  }
  public void onClickClose(@SuppressWarnings("unused") View view) {
    listFragment.getListView().clearChoices();
    listFragment.getListView().requestLayout();
    videoFragment.pause();
    ViewPropertyAnimator animator = videoBox.animate()
        .translationYBy(videoBox.getHeight())
        .setDuration(ANIMATION_DURATION_MILLIS);
    runOnAnimationEnd(animator, new Runnable() {
      @Override
      public void run() {
        videoBox.setVisibility(View.INVISIBLE);
      }
    });
  }
  public void onClickPlayNow(@SuppressWarnings("unused") View view) {
	  Intent itent = new Intent(VideoListDemoActivity.this, PlayerViewDemoActivity.class);
		itent.putExtra("videoId", videoFragment.videoId);
		startActivity(itent);
  }
  @TargetApi(16)
  private void runOnAnimationEnd(ViewPropertyAnimator animator, final Runnable runnable) {
    if (Build.VERSION.SDK_INT >= 16) {
      animator.withEndAction(runnable);
    } else {
      animator.setListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
          runnable.run();
        }
      });
    }
  }

  /**
   * A fragment that shows a static list of videos.
   */
  public static final class VideoListFragment extends ListFragment {
	
    static {
      List<VideoEntry> list = new ArrayList<VideoEntry>();
      list.add(new VideoEntry("Tuyển Tập Những Bài Hát Hay V�? Huế", "f_DfnmIGlp8"));
      VIDEO_LIST = Collections.unmodifiableList(list);
    }

    private PageAdapter adapter;
    private View videoBox;
    private String nextPage = "";
    private String prevPage = "";
    private String query = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      //adapter = new PageAdapter(getActivity(), VIDEO_LIST);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
      super.onActivityCreated(savedInstanceState);

      videoBox = getActivity().findViewById(R.id.video_box);
      getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
      setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
      String videoId = VIDEO_LIST.get(position).getVideoId();

      VideoFragment videoFragment =
          (VideoFragment) getFragmentManager().findFragmentById(R.id.video_fragment_container);
      videoFragment.setVideoId(videoId);

      // The videoBox is INVISIBLE if no video was previously selected, so we need to show it now.
      if (videoBox.getVisibility() != View.VISIBLE) {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
          // Initially translate off the screen so that it can be animated in from below.
          videoBox.setTranslationY(videoBox.getHeight());
        }
        videoBox.setVisibility(View.VISIBLE);
      }

      // If the fragment is off the screen, we animate it in.
      if (videoBox.getTranslationY() > 0) {
        videoBox.animate().translationY(0).setDuration(ANIMATION_DURATION_MILLIS);
      }
    }

    @Override
    public void onDestroyView() {
      super.onDestroyView();
      
      //adapter.releaseLoaders();
    }

    public void setLabelVisibility(boolean visible) {
    	if (adapter != null){
      adapter.setLabelVisibility(visible);
      }
    }

	public String getNextPage() {
		return nextPage;
	}

	public void setNextPage(String nextPage) {
		this.nextPage = nextPage;
	}

  }

  /**
   * Adapter for the video list. Manages a set of YouTubeThumbnailViews, including initializing each
   * of them only once and keeping track of the loader of each one. When the ListFragment gets
   * destroyed it releases all the loaders.
   */
  private static final class PageAdapter extends BaseAdapter {

    private final List<VideoEntry> entries;
    private final List<View> entryViews;
    private final Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap;
    private final LayoutInflater inflater;
    private final ThumbnailListener thumbnailListener;

    private boolean labelsVisible;

    public PageAdapter(Context context, List<VideoEntry> entries) {
      this.entries = entries;

      entryViews = new ArrayList<View>();
      thumbnailViewToLoaderMap = new HashMap<YouTubeThumbnailView, YouTubeThumbnailLoader>();
      inflater = LayoutInflater.from(context);
      thumbnailListener = new ThumbnailListener();

      labelsVisible = true;
    }

    public void releaseLoaders() {
      for (YouTubeThumbnailLoader loader : thumbnailViewToLoaderMap.values()) {
        loader.release();
      }
    }

    public void setLabelVisibility(boolean visible) {
      labelsVisible = visible;
      for (View view : entryViews) {
        view.findViewById(R.id.text).setVisibility(visible ? View.VISIBLE : View.GONE);
      }
    }

    @Override
    public int getCount() {
      return entries.size();
    }

    @Override
    public VideoEntry getItem(int position) {
      return entries.get(position);
    }

    @Override
    public long getItemId(int position) {
      return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      View view = convertView;
      VideoEntry entry = entries.get(position);

      // There are three cases here
      if (view == null) {
        // 1) The view has not yet been created - we need to initialize the YouTubeThumbnailView.
        view = inflater.inflate(R.layout.video_list_item, parent, false);
        YouTubeThumbnailView thumbnail = (YouTubeThumbnailView) view.findViewById(R.id.thumbnail);
        thumbnail.setTag(entry.getVideoId());
        thumbnail.initialize(DeveloperKey.DEVELOPER_KEY, thumbnailListener);
      } else {
        YouTubeThumbnailView thumbnail = (YouTubeThumbnailView) view.findViewById(R.id.thumbnail);
        YouTubeThumbnailLoader loader = thumbnailViewToLoaderMap.get(thumbnail);
        if (loader == null) {
          // 2) The view is already created, and is currently being initialized. We store the
          //    current videoId in the tag.
          thumbnail.setTag(entry.getVideoId());
        } else {
          // 3) The view is already created and already initialized. Simply set the right videoId
          //    on the loader.
          thumbnail.setImageResource(R.drawable.loading_thumbnail);
          loader.setVideo(entry.getVideoId());
        }
      }
      TextView label = ((TextView) view.findViewById(R.id.text));
      label.setText(entry.getText());
      label.setVisibility(labelsVisible ? View.VISIBLE : View.GONE);
      return view;
    }

    private final class ThumbnailListener implements
        YouTubeThumbnailView.OnInitializedListener,
        YouTubeThumbnailLoader.OnThumbnailLoadedListener {

      @Override
      public void onInitializationSuccess(
          YouTubeThumbnailView view, YouTubeThumbnailLoader loader) {
        loader.setOnThumbnailLoadedListener(this);
        thumbnailViewToLoaderMap.put(view, loader);
        view.setImageResource(R.drawable.loading_thumbnail);
        String videoId = (String) view.getTag();
        loader.setVideo(videoId);
      }

      @Override
      public void onInitializationFailure(
          YouTubeThumbnailView view, YouTubeInitializationResult loader) {
        view.setImageResource(R.drawable.no_thumbnail);
      }

      @Override
      public void onThumbnailLoaded(YouTubeThumbnailView view, String videoId) {
      }

      @Override
      public void onThumbnailError(YouTubeThumbnailView view, ErrorReason errorReason) {
        view.setImageResource(R.drawable.no_thumbnail);
      }
    }

  }

  public static final class VideoFragment extends YouTubePlayerFragment
      implements OnInitializedListener {

    private YouTubePlayer player;
    private String videoId;

    public static VideoFragment newInstance() {
      return new VideoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      initialize(DeveloperKey.DEVELOPER_KEY, this);
    }

    @Override
    public void onDestroy() {
      if (player != null) {
        player.release();
      }
      super.onDestroy();
    }

    public void setVideoId(String videoId) {
      if (videoId != null && !videoId.equals(this.videoId)) {
        this.videoId = videoId;
        if (player != null) {
          player.cueVideo(videoId);
        }
      }
    }

    public void pause() {
      if (player != null) {
        player.pause();
      }
    }

    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean restored) {
      this.player = player;
      player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
      player.setOnFullscreenListener((VideoListDemoActivity) getActivity());
      if (!restored && videoId != null) {
        player.cueVideo(videoId);
      }
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult result) {
      this.player = null;
    }

  }


  // Utility methods for layouting.

  private int dpToPx(int dp) {
    return (int) (dp * getResources().getDisplayMetrics().density + 0.5f);
  }

  private static void setLayoutSize(View view, int width, int height) {
    LayoutParams params = view.getLayoutParams();
    params.width = width;
    params.height = height;
    view.setLayoutParams(params);
  }

  private static void setLayoutSizeAndGravity(View view, int width, int height, int gravity) {
    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
    params.width = width;
    params.height = height;
    params.gravity = gravity;
    view.setLayoutParams(params);
  }
  public static String getUrl(String query, String pageToken, String key){
	  StringBuilder builder = new StringBuilder();
	  builder.append("https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&key=");
	  builder.append(key);
	  builder.append("&q=");
	  builder.append(query);
	  builder.append("&pageToken=");
	  builder.append(pageToken);
	  builder.append("&maxResults=20");
	  return builder.toString();
  }
  class GetMovies extends AsyncTask<String, Void, JsonNode>{
		@Override
		protected JsonNode doInBackground(String... params) {
			return HttpUtil.getJson(params[0]);
		}
		
		@Override
		protected void onPostExecute(JsonNode result) {
			if (result != null){
				if (result.get("nextPageToken") != null){
					listFragment.nextPage = result.get("nextPageToken").asText();
				}else{
					listFragment.nextPage = "";
				}
				if (result.get("prevPageToken") != null){
					listFragment.prevPage = result.get("prevPageToken").asText();
				}else{
					listFragment.prevPage = "";
				}
				movies.clear();
				movies = new ArrayList<VideoEntry>();
				JsonNode items = result.get("items");
				Iterator<JsonNode> ite = items.iterator();
				while(ite.hasNext()){
					movies.add(Converter.json2Youtube(ite.next()));
				}
				VIDEO_LIST = Collections.unmodifiableList(movies);
				PageAdapter adapter = new PageAdapter(listFragment.getActivity(), VIDEO_LIST);
				//listFragment.adapter.releaseLoaders();
				listFragment.setListAdapter(adapter);
			}
			hidePleaseWait();
		}
	}


@Override
public void RibbonMenuItemClick(int itemId) {
	loadAdview();
	GetMovies gm = new GetMovies();
	String st = "";
	String query = "";
	switch (itemId) {
		case R.id.thuynga:
			query = "Paris+by+night+Thuy+nga";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
		break;
		case R.id.ctt:
			query = "Ch%C3%A2u+Tinh+Tr%C3%AC";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.vanson:
			query = "V%C3%A2n+S%C6%A1n+Entertaiment";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.taoquan:
			query = "T%C3%A1o+Qu%C3%A2n";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.ongioi:
			query = "%C6%A0n+Gi%E1%BB%9Di+C%E1%BA%ADu+%C4%91%C3%A2y+r%E1%BB%93i";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.nhacxuan:
			query = "Nh%E1%BA%A1c+xu%C3%A2n";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.cliphai:
			query = "haivl";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.hoailinh:
			query = "hoai+linh";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.truonggiang:
			query = "Truong+Giang";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.chitai:
			query = "Ch%C3%AD+t%C3%A0i";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.tranthanh:
			query = "Tr%E1%BA%A5n+Th%C3%A0nh";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.tn:
			query = "Th%C3%BAy+Nga";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.vsbl:
			query = "V%C3%A2n+S%C6%A1n+B%E1%BA%A3o+Li%C3%AAm";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.xuanhinh:
			query = "Xu%C3%A2n+Hinh";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.xuanbac:
			query = "Xu%C3%A2n+B%E1%BA%AFc";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.congly:
			query = "C%C3%B4ng+L%C3%BD";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.tulong:
			query = "T%E1%BB%B1+Long";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.catphuong:
			query = "C%C3%A1t+Ph%C6%B0%E1%BB%A3ng";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		case R.id.viethuong:
			query = "Vi%E1%BB%87t+H%C6%B0%C6%A1ng";
			st = VideoListDemoActivity.getUrl(query, "", DeveloperKey.DEVELOPER_KEY);
			showPleaseWait();
			gm.execute(st);
			break;
		}
	listFragment.query = query;
	}
}
