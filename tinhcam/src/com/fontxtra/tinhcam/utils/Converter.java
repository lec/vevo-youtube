package com.fontxtra.tinhcam.utils;

import com.fontxtra.tinhcam.adapter.VideoEntry;
import com.fasterxml.jackson.databind.JsonNode;

public class Converter {
	
	public static VideoEntry json2Youtube(JsonNode m){
		VideoEntry movie = new VideoEntry();
		JsonNode snippet = m.get("snippet");
		if (snippet != null){
			if (snippet.get("title") != null)
			{
				movie.setText(snippet.get("title").asText());
			}
			if (snippet.get("resourceId") != null)
			{
				movie.setVideoId(snippet.get("resourceId").get("videoId").asText());
			}
		}
		JsonNode id = m.get("id");
		if (id != null){
			if (id.get("videoId") != null){
				movie.setVideoId(id.get("videoId").asText());
			}
		}
		return movie;
	}

}
